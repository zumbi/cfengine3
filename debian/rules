#!/usr/bin/make -f
# vim:noet:ts=4:sw=4:

include /usr/share/dpkg/buildflags.mk
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

export EXPLICIT_VERSION = $(shell dpkg-parsechangelog -S version | cut  -f 1 -d -)
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

# Exporting V=1 so that we can see build flags in the output.
export V=1

%:
	dh $@

# TODO(antonio): disable sql in experimental as postgresql is not ready for
# openssl1.1 transition, re-enable once it becomes ready.
override_dh_auto_configure:
	dh_auto_configure -- \
			--enable-fhs \
			--libdir=/usr/lib \
			--with-logdir=/var/log/cfengine3 \
			--with-piddir=/var/run/cfengine3 \
			--with-workdir=/var/lib/cfengine3 \
			--without-postgresql \
			--without-mysql \
			--with-libvirt \
			--with-lmdb \
			--with-libxml2 \
			--with-libyaml
	
	dh_auto_configure --sourcedirectory=masterfiles -- \
	        --prefix=/usr/share/cfengine3 \
			--with-core=$(CURDIR)

override_dh_clean:
	dh_clean --exclude=#008.cf#

override_dh_installchangelogs:
	dh_installchangelogs --exclude=ChangeLog

override_dh_auto_clean:
	dh_auto_clean
	dh_auto_clean -Dmasterfiles

override_dh_compress:
	dh_compress -X.cf

override_dh_strip:
	dh_strip --dbgsym-migration='cfengine3-dbg (<< 3.10.2-3~)'

override_dh_installman:
	## Man pages are created with an undocumented flag
	/bin/mkdir -p $(CURDIR)/debian/cfengine3/usr/share/man/man8/
	for p in cf-agent cf-key cf-promises cf-runagent cf-execd cf-serverd cf-monitord; \
		do $(CURDIR)/$$p/$$p -M > $(CURDIR)/debian/cfengine3/usr/share/man/man8/$$p.8 ;\
        done
	dh_installman

override_dh_auto_install:
	dh_auto_install
	dh_auto_install -Dmasterfiles

override_dh_install:
	dh_install --exclude=examples --exclude=ChangeLog
